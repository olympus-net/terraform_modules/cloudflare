locals {
  z_id = index(data.cloudflare_zones.zone.zones.*.name, var.zone)
}

data "cloudflare_zones" "zone" {
  filter {
    name   = var.zone
    status = "active"
    paused = false
  }
}

// Create an A record
resource "cloudflare_record" "A_records" {
  // Only create this type if the record_type is matching.
  count = var.record_type == "A" ? 1 : 0

  zone_id = data.cloudflare_zones.zone.zones.*.id[local.z_id]
  name    = var.record.name
  value   = var.record.content
  type    = "A"
  ttl     = var.ttl
  proxied = var.proxied
}

// Create a CNAME record
resource "cloudflare_record" "CNAME_records" {
  // Only create this type if the record_type is matching.
  count = var.record_type == "CNAME" ? 1 : 0

  zone_id = data.cloudflare_zones.zone.zones.*.id[local.z_id]
  name    = var.record.name
  value   = var.record.content
  type    = "CNAME"
  ttl     = 1
  proxied = var.proxied
}

// Create a TXT record
resource "cloudflare_record" "TXT_records" {
  // Only create this type if the record_type is matching.
  count = var.record_type == "TXT" ? 1 : 0

  zone_id = data.cloudflare_zones.zone.zones.*.id[local.z_id]
  name    = var.record.name
  value   = var.record.content
  type    = "TXT"
  ttl     = 1
}


// Create a MX record
resource "cloudflare_record" "MX_records" {
  // Only create this type if the record_type is matching.
  count = var.record_type == "MX" ? 1 : 0

  zone_id = data.cloudflare_zones.zone.zones.*.id[local.z_id]
  name    = var.record.name
  value   = var.record.content
  type    = "MX"
  ttl     = 1
}
