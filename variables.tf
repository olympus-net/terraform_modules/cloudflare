variable "zone" {
  type = string

  description = "Cloudflare zone to query."
}

variable "record" {
  type = object({
    name    = string
    content = string
  })

  description = <<EOD
Records to update in the expected format. All records are expected to be of like kind.
  {
    name    = "some-record1"
    content = "192.168.0.1"
  }

Or example of CNAME
  {
    name    = "some-record1"
    content = "point_here.some_domain.org"
  }

Or example of TXT
  {
    name    = "some-record1"
    content = "v=spf1 include:somethinghere"
  }
EOD
}


# Optional

variable "record_type" {
  type = string

  description = "Type of record to create or update."
  default     = "A"

  validation {
    condition     = can(index(["A", "CNAME", "TXT", "MX"], upper(var.record_type)))
    error_message = "Record Type must be one of: A, CNAME, TXT or MX."
  }
}

variable "ttl" {
  type = number

  description = "Time To Live time in seconds. Settitng this to 1 uses Cloudflare's automatic setting."

  default = 1
}

variable "proxied" {
  type    = bool
  default = false
}
