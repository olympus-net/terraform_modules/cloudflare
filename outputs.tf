output "dns_record" {
  value = {
    for r in concat(
      cloudflare_record.A_records,
      cloudflare_record.CNAME_records,
      cloudflare_record.TXT_records,
      cloudflare_record.MX_records
    ) :
    r.id => {
      name     = r.name
      hostname = r.hostname
      value    = r.value
      type     = r.type
      proxied  = r.proxied
      ttl      = r.ttl
    }
  }

  description = "Map of any and all DNS records created."
}

output "zones" {
  value = data.cloudflare_zones.zone.zones.*.name

  description = "Zones discovered."
}

output "zones_used" {
  value = data.cloudflare_zones.zone.zones.*.id[local.z_id]

  description = "DNS zone used when actually creating the record. (Mostly useful for debugging)"
}
